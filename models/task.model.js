module.exports = (sequelize, Sequelize) => {
    /*  Columns: id, task_name, description, task_type, weight, notes, published, createdAt, updatedAt */
    const Task = sequelize.define("task", {
      task_name: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.STRING
      },
      task_type: {
        type: Sequelize.STRING
      },
      weight: {
        type: Sequelize.INTEGER
      },
      notes: {
        type: Sequelize.STRING
      },
      published: {
        type: Sequelize.BOOLEAN
      }
    });
  
    return Task;
  };

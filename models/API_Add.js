/*
PostMan API client
------
POST	http://localhost:8080/api/tasks   
GET 	http://localhost:8080/api/tasks  

UPDATE 	http://localhost:8080/api/tasks/6 

Old: "description": "Description of task 6 Month Demo Update." 
New: "description": "Description of task 6 Month Demo Update. - Now Updated" 

*/

//  http://localhost:8080/api/tasks  

{
    "task_name": "New Recruit: Shipped, Not received (HH status 08 03)", 
    "description": "Description of task 08 03.",  
	"task_type": "Automated", 
	"weight": 700, 
    "published": false 
},
{
    "task_name": "New Recruit: Welcomed, Not installed (HH status 08 04)", 
    "description": "Description of task 08 04.",  
	"task_type": "Automated", 
	"weight": 650, 
    "published": false 
},
{
    "task_name": "New recruit: Installed, Not Welcomed (HH status 08 08)", 
    "description": "Description of task 08 08 to be changed",  
	"task_type": "Automated", 
	"weight": 750, 
    "published": false 
},
{
    "task_name": "Home Due a 3 Month Thank You Call", 
    "description": "Description of task 3 Month Demo Update",  
	"task_type": "SharePoint", 
	"weight": 170, 
    "published": false 
},
{
    "task_name": "Individual(s) in Vacation HH's with Minimum Motion (work order 458 & 459)", 
    "description": "Description of task 458 & 459",  
	"task_type": "Automated", 
	"weight": 8000, 
    "published": false 
},
{
    "task_name": "Home Due a 6 Month Thank You Call", 
    "description": "Description of task 6 Month Demo Update. - Now Updated",  
	"task_type": "SharePoint", 
	"weight": 160, 
    "published": false 
}


// Update | PUT  -->  http://localhost:8080/api/tasks/6  
{
    "description": "Description of task 6 Month Demo Update. - Now Updated" 
}

// Find all Tasks where Published is True 
// GET --> http://localhost:8080/api/tasks?published=true 

// Find all Tasks containing the word 'Task'  
// GET --> http://localhost:8080/api/tasks?task_name=Welcomed 


/*
USE singlesourcedb;
-- DROP TABLE tasks; 
SELECT * FROM tasks; 
*/









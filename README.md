# Numeris Single Source Task List
A RESTful API back-end for Numeris Single Source Task List, using a MySQL RDBMS via Sequelize ORM, using a Node.js and Express back-end, for an Angular front-end.  Enjoy! 

![Numeris Single Source Task List ~ Info](http://www.ryanhunter.ca/images/portfolio/single_source_api/screen04.png)


## Steps to Install
- git clone https://systemsvanguard@bitbucket.org/systemsvanguard/single_source_api.git
- cd single_source_api\ 
- npm install
- Very important! Ensure to rename local ".env4Display" to ".env".  It contains database configuration info.
- Start the server with "node server.js" OR "nodemon server.js" 
- Runs on port 8080 --> http://localhost:8080/


## Features
- Node.js
- Express.js
- Handlebars
- RESTful API design
- Postman client screen-shots 
- MySQL RDBMS
- Sequelize DB ORM


## License
This project is licensed under the terms of the **MIT** license.


## Screenshots
![Screen 01](http://www.ryanhunter.ca/images/portfolio/single_source_api/screen01.png)
![Screen 02](http://www.ryanhunter.ca/images/portfolio/single_source_api/screen02.png)
![Screen 03](http://www.ryanhunter.ca/images/portfolio/single_source_api/screen03.png)
![Screen 04](http://www.ryanhunter.ca/images/portfolio/single_source_api/screen04.png)
![Screen 05](http://www.ryanhunter.ca/images/portfolio/single_source_api/screen05.png)

// starting point for RESTful API
require('dotenv').config();
const express = require ("express");
const cors = require("cors");
const path = require('path');
const port = process.env.PORT || 8080;
const appPort = process.env.PORT || 8080;
const apiPort = "http://localhost:8081/";
const exphbs = require('express-handlebars');

// create app
const app = express();

var corsOptions = {
    origin: apiPort 
};

app.use(cors(corsOptions));

// -------------------------------
/* Use View Engine.  Must come before routing. */
app.engine('handlebars', exphbs() );
app.set('view engine', 'handlebars' );

// parse requests of content type ~ application/json & application/x-www-form-urlencoded 
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// -------------------------------
// database connection
const db = require("./models"); 

db.sequelize.sync();
/*  
	// Drop the table if it already exists  
    db.sequelize.sync({ force: true }).then(() => {
        console.log("Truncate/ Drop and re-sync the database.");
    });
*/
// -------------------------------


// app page routing
app.get('/', function(req, res) {
  res.render('home', {
    tagline:siteTagline,
    pageTitle : "Numeris Single Source Task List RESTful API prototype"
  });
} );

app.get('/about', function(req, res) {
  res.render('about');
} );

app.get('/contact', function(req, res) {
  res.render('contact');
} );

app.get('/services', function(req, res) {
  res.render('services');
} );

// API routing
require("./routes/task.routes.js")(app);

/*
app.get("/", (req, res) => {
    res.json({message: "Numeris Single Source Task List RESTful API prototype. Uses a MySQl RDBMS back-end, with Sequelize ORM."});
} );
*/
// -------------------------------


// configure static files location
// app.use(express.static((__dirname + '/public' )) );
app.use(express.static(path.join((__dirname + '/public' ))) );

// universal variables
const siteTagline = "Shaping Canada's Media Landscape";

// -------------------------------
// start web server & listen on port
const PORT = process.env.PORT || appPort;
app.listen(PORT, () => {
    console.log(`RESTful API web application now running on port ${PORT} at http://localhost:${PORT}/ . Enjoy! `)
}  );
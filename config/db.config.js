// DB connection settings

// 'max' & other times below are time in milli-seconds.
/* API reference for Sequelize ORM constructor : https://sequelize.org/master/class/lib/sequelize.js~Sequelize.html#instance-constructor-constructor   */
module.exports = {
    HOST: "localhost", 
    USER: process.env.USER,
    PASSWORD: process.env.PASSWORD,
    DB: process.env.DB ,
    dialect: "mysql",
    pool: {
        max: 5, 
        min: 0, 
        acquire: 30000, 
        idle: 10000
    }
};
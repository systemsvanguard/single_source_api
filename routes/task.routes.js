module.exports = app => {
    const tasks = require("../controllers/task.controller.js");    
    var router = require("express").Router();
  
    /*  CRUD == Create, Read, Update, & Delete   - see related routes & methods below.    */
    
    // Create a new Task
    router.post("/", tasks.create);

  
    // Retrieve all tasks
    router.get("/", tasks.findAll);
  
    // Retrieve all published tasks
    router.get("/published", tasks.findAllPublished);
  
    // Retrieve a single Task by ID 
    router.get("/:id", tasks.findOne);

  
    // Update a Task by ID
    router.put("/:id", tasks.update);

  
    // Delete a Task by ID
    router.delete("/:id", tasks.delete);
  
    // Delete all tasks
    router.delete("/", tasks.deleteAll);

    //---------------------  
    app.use('/api/tasks', router);
  };
